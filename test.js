var page = require('webpage').create();
page.settings.resourceTimeout = 10000;

function padDigits(number, digits) {
    return Array(Math.max(digits - String(number).length + 1, 0)).join(0) + number;
}

console.log("opening page...")
page.open('https://www.monkeybrains.net/support.php', function(status){
  console.log('Status: ' + status);
  const startTime = new Date();
  const startTimeString = startTime.toISOString().split('T')[0]+'.'+
                          padDigits(startTime.getHours(),2)+'-'+
                          padDigits(startTime.getMinutes(),2)+'-'+
                          padDigits(startTime.getSeconds(),2);

  //give the script some time to run
  setTimeout(function(){ 
    var dl = 'err';
    var ul = 'err';

    try {
      dl = page.evaluate(function() {
        return document.getElementById('dl').textContent || 'err';
      });
      ul = page.evaluate(function() {
        return document.getElementById('ul').textContent || 'err';
      });
    } catch (ex) {
      console.log(ex);
    }
    
    const fname = 'speedtest-'+startTimeString+'.dl-'+dl+'-ul-'+ul+'.png';
    page.render(fname);
    phantom.exit();
  }, 20000);
  
});
